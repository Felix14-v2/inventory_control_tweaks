package net.sssubtlety.inventory_control_tweaks;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import java.util.List;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.Gui.Tooltip
    public boolean enable_armor_swap = FeatureControl.Defaults.enable_armor_swap;

    @ConfigEntry.Gui.Tooltip
    public boolean equip_and_swap_non_armor_wearables = FeatureControl.Defaults.equip_and_swap_non_armor_wearables;
    @ConfigEntry.Gui.PrefixText

    @ConfigEntry.Gui.Tooltip
    public final List<String> armor_swap_deny_list = FeatureControl.Defaults.armor_swap_deny_list;

    @ConfigEntry.Gui.Tooltip
    public boolean fetch_translation_updates = FeatureControl.Defaults.fetch_translation_updates;

    @ConfigEntry.Gui.Tooltip
    public boolean enable_pick_fills_stack = FeatureControl.Defaults.enable_pick_fills_stack;

    @ConfigEntry.Gui.Tooltip
    public boolean enable_pick_never_changes_slot = FeatureControl.Defaults.enable_pick_never_changes_slot;

    // drag_item_tweaks
    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean drag_matching_stacks_across_inventories = FeatureControl.Defaults.drag_matching_stacks_across_inventories;
    @ConfigEntry.Gui.PrefixText

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean choose_bottom_row_stacks_first = FeatureControl.Defaults.choose_bottom_row_stacks_first;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public FeatureControl.DepositType deposit_cursor_stack_on_release = FeatureControl.Defaults.deposit_cursor_stack_on_release;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean drag_single_stack_out_of_inventory = FeatureControl.Defaults.drag_single_stack_out_of_inventory;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public FeatureControl.Keyable drag_matching_stacks_out_of_inventory = FeatureControl.Defaults.drag_matching_stacks_out_of_inventory;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean ignore_stack_nbt = FeatureControl.Defaults.ignore_stack_nbt;

    // offhand tweaks
    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean enable_shift_click_to_offhand_stack = FeatureControl.Defaults.enable_shift_click_to_offhand_stack;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean all_food_is_offhand_preferred = FeatureControl.Defaults.all_food_is_offhand_preferred;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean exclude_food_with_negative_effects = FeatureControl.Defaults.exclude_food_with_negative_effects;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public final List<String> offhand_preferred_items = FeatureControl.Defaults.offhand_preferred_items;
}
