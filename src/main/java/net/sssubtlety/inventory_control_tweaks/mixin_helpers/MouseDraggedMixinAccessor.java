package net.sssubtlety.inventory_control_tweaks.mixin_helpers;

import net.minecraft.inventory.Inventory;

public interface MouseDraggedMixinAccessor {
    Inventory inventory_control_tweaks$getSourceInventory();
    Inventory inventory_control_tweaks$getDestinationInventory();
    boolean inventory_control_tweaks$hasMovedStacks();
    boolean inventory_control_tweaks$hasCursorCrossedInventories();
    void inventory_control_tweaks$endMove();
}
