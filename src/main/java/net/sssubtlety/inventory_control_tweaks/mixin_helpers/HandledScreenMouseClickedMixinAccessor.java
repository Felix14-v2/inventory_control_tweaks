package net.sssubtlety.inventory_control_tweaks.mixin_helpers;

public interface HandledScreenMouseClickedMixinAccessor {
    boolean inventory_control_tweaks$getDidReplaceMouseClickSlot();
}
