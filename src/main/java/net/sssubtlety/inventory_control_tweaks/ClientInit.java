package net.sssubtlety.inventory_control_tweaks;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;

public class ClientInit implements ClientModInitializer {
    static {
        FeatureControl.init();
        if (FeatureControl.shouldFetchTranslationUpdates())
            CrowdinTranslate.downloadTranslations("inventory-control-tweaks", InventoryControlTweaks.NAMESPACE);
    }

    @Override
    public void onInitializeClient() { }
}
