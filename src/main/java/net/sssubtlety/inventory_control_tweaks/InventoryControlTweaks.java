package net.sssubtlety.inventory_control_tweaks;

// TODO:
//  - prevent drag across depositing to offhand
//  - test why armor is dragged with cammie's wearable backpacks
//  - add 'swap cursor stack with matching full stacks' option
//  - add option to make shift-clicking start with hotbar slot 0 instead of 8
public class InventoryControlTweaks {
	public static final String NAMESPACE = "inventory_control_tweaks";
	public static final Util.TranslatableString NAME = new Util.TranslatableString("text." + NAMESPACE + ".name");
}
