package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.sssubtlety.inventory_control_tweaks.CreativeSlotCheckable;
import net.sssubtlety.inventory_control_tweaks.FeatureControl;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.HandledScreenMouseClickedMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.sssubtlety.inventory_control_tweaks.FeatureControl.clickArmorSwapModifier;
import static net.sssubtlety.inventory_control_tweaks.Util.*;

@Mixin(HandledScreen.class)
public abstract class HandledScreenMouseClickedMixin<T extends ScreenHandler> extends Screen implements ScreenHandlerProvider<T>, HandledScreenMouseClickedMixinAccessor {
    private boolean inventory_control_tweaks$didReplaceMouseClickSlot;

    @Shadow protected abstract void onMouseClick(Slot slot, int invSlot, int clickData, SlotActionType actionType);

    @Shadow public abstract T getScreenHandler();

    @Shadow protected Slot focusedSlot;

    protected HandledScreenMouseClickedMixin(Text title) {
        super(title);
        throw new IllegalStateException("HandledScreenMixin's dummy constructor called! ");
    }

    @ModifyArg(
        method = "mouseClicked",
        at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;onMouseClick(Lnet/minecraft/screen/slot/Slot;IILnet/minecraft/screen/slot/SlotActionType;)V"),
        slice = @Slice(
            from = @At(value = "FIELD", target = "Lnet/minecraft/screen/slot/SlotActionType;THROW:Lnet/minecraft/screen/slot/SlotActionType;"),
            to = @At(value = "FIELD", target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;heldButtonCode:I")
        )
    )
    private Slot inventory_control_tweaks$preMouseClickSlot(Slot slot, int invSlot, int clickData, SlotActionType slotActionType) {
        inventory_control_tweaks$didReplaceMouseClickSlot =
                slot != null &&
                // left click
                clickData == 0 &&
                // [own inventory or creative inventory without creative tab open]
                (this.client.player.playerScreenHandler == this.client.player.currentScreenHandler ||
                    (
                        ((this.client.player.currentScreenHandler instanceof CreativeInventoryScreen.CreativeScreenHandler) &&
                        ((CreativeInventoryScreen.CreativeScreenHandler) this.client.player.currentScreenHandler).itemList.size() == 0)
                    )
                ) &&
                // goes to offhand
                (   // shift-click to offhand
                    (slotActionType == SlotActionType.QUICK_MOVE && inventory_control_tweaks$tryMovingToOffhand(slot)) ||
                    // armor swap
                    (
                        isKeyPressed(clickArmorSwapModifier, this.client) &&
                        (FeatureControl.shouldEquipAndSwapNonArmorWearables() || slot.getStack().getItem() instanceof ArmorItem) &&
                        tryEquipItem(this.client.player, slot.getStack(), getClickedStackInd(slot instanceof CreativeSlotCheckable ? ((CreativeSlotAccessor) slot).inventory_control_tweaks$getSlot().id : slot.id))
                    )
                );
        // no change, just using ModifyArg to capture method args
        return slot;
    }

    // same mixin is in CreativeInventoryScreenMixin because CreativeInventoryScreen @Overrides onMouseClick
    @Inject(
        method = "onMouseClick(Lnet/minecraft/screen/slot/Slot;IILnet/minecraft/screen/slot/SlotActionType;)V",
        cancellable = true,
        at = @At(value = "HEAD")
    )
    private void inventory_control_tweaks$cancelMouseClickSlotIfReplaced(Slot slot, int slotId, int button, SlotActionType actionType, CallbackInfo ci) {
        if (inventory_control_tweaks$didReplaceMouseClickSlot) ci.cancel();
    }

    // returns true if alternate behavior was successful (meaning vanilla behavior should not also happen)
    private boolean inventory_control_tweaks$tryMovingToOffhand(Slot slot) {
        if (this.client == null ||
                this.client.interactionManager == null ||
                this.client.player == null)
            return false;

        if (slot.inventory != this.client.player.getInventory()) return false;

        if (this.client.player.isCreative()) {
            if (slot.getIndex() == 45) return false;
        } else if (slot.getIndex() == PlayerInventory.OFF_HAND_SLOT) return false;

        ItemStack offHandStack = this.client.player.getOffHandStack();
        ItemStack clickedStack = slot.getStack();

        if (offHandStack.isEmpty()) {
            Item item = clickedStack.getItem();
            if (FeatureControl.isOffhandPreferred(item) ||
                    (FeatureControl.isAllFoodOffhandPreferred() && isValidFood(item))
            ) {
                // swap with (empty) offhand
                this.onMouseClick(this.focusedSlot, this.focusedSlot.id, PlayerInventory.OFF_HAND_SLOT, SlotActionType.SWAP);
                this.getScreenHandler().sendContentUpdates();
                return true;
            }
            return false;
        }


        if (FeatureControl.shouldShiftClickToOffhandStack()) {
            if (stackIsFull(offHandStack))
                // no room in stack in offHand
                return false;

            if (!stacksMatch(offHandStack, clickedStack, false))
                // stacks can't combine
                return false;

            final int index = this.focusedSlot.getIndex();
            final int adjustedId = getClickedSlotId(index);
            this.onMouseClick(null, adjustedId, 0, SlotActionType.PICKUP);
            this.onMouseClick(null, 45, 0, SlotActionType.PICKUP);
            this.onMouseClick(null, adjustedId, 0, SlotActionType.PICKUP);

            return true;
        }

        return false;
    }

    public boolean inventory_control_tweaks$getDidReplaceMouseClickSlot() {
        return inventory_control_tweaks$didReplaceMouseClickSlot;
    }
}
