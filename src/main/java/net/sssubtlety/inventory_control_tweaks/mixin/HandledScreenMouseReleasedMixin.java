package net.sssubtlety.inventory_control_tweaks.mixin;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.sssubtlety.inventory_control_tweaks.FeatureControl;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.MouseDraggedMixinAccessor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.sssubtlety.inventory_control_tweaks.FeatureControl.dragMatchingOutOfInventoryModifier;
import static net.sssubtlety.inventory_control_tweaks.Util.*;

@Mixin(HandledScreen.class)
public abstract class HandledScreenMouseReleasedMixin <T extends ScreenHandler> extends Screen implements ScreenHandlerProvider<T> {
    private static final int inventory_control_tweaks$NO_SLOT_INDICATOR = -999;

    private MouseDraggedMixinAccessor inventory_control_tweaks$draggedAccessor;

    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;

    protected HandledScreenMouseReleasedMixin(Text title) {
        super(title);
        throw new IllegalStateException("HandledScreenMouseReleasedMixin's dummy constructor called!");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    void inventory_control_tweaks$postConstruction(CallbackInfo ci) {
        inventory_control_tweaks$draggedAccessor = (MouseDraggedMixinAccessor) this;
    }

    @Inject(method = "mouseReleased", locals = LocalCapture.CAPTURE_FAILHARD, at = @At(value = "FIELD", ordinal = 2,
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;cancelNextRelease:Z"))
    void inventory_control_tweaks$onMouseReleaseCancel(double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> cir, Slot hoveredSlot, int i, int j, boolean bl, int slotIndicator) {
        if (this.client == null ||
                this.client.player == null ||
                button != 0 ||
                this.doubleClicking) return;

        final ItemStack cursorStack = this.client.player.currentScreenHandler.getCursorStack();

        if (slotIndicator == inventory_control_tweaks$NO_SLOT_INDICATOR) {
            inventory_control_tweaks$onReleaseOutsideBounds(cursorStack);
        } else {
            inventory_control_tweaks$onReleaseInsideBounds(hoveredSlot, cursorStack);
        }

        // reset unconditionally at end
        inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$endMove();

    }

    private void inventory_control_tweaks$onReleaseOutsideBounds(ItemStack cursorStack) {
        if (cursorStack.isEmpty()) return;

        final FeatureControl.Keyable shouldDragMatchingStacksOutOfInventory = FeatureControl.shouldDragMatchingStacksOutOfInventory();
        boolean anyThrowKeyHeld = false;
        final boolean shouldThrowMatchingStacks = shouldDragMatchingStacksOutOfInventory == FeatureControl.Keyable.NO_KEY ||
                (anyThrowKeyHeld = (shouldDragMatchingStacksOutOfInventory == FeatureControl.Keyable.WITH_KEY &&
                        isKeyPressed(dragMatchingOutOfInventoryModifier, this.client)));

        final boolean shouldThrowAllStacks = (anyThrowKeyHeld |= isKeyPressed(FeatureControl.dragAllOutOfInventoryModifier, this.client));

        if (inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$hasMovedStacks() && !anyThrowKeyHeld) {
            // if stacks moved and no throw key held, prevent throwing and instead deposit cursor stack
            // this is to prevent accidental throwing (a key being held is taken to indicate it isn't an accident)
            inventory_control_tweaks$depositRemainder(cursorStack, inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$getDestinationInventory());
            return;
        }

        if (!(
                shouldThrowMatchingStacks ||
                FeatureControl.shouldDragSingleStackOutOfInventory() ||
                shouldThrowAllStacks
            )) return;

        // throw cursor stack
        final boolean isCreativeInv = this.client.player.currentScreenHandler instanceof CreativeInventoryScreen.CreativeScreenHandler;
        if (isCreativeInv) {
            this.client.player.dropItem(cursorStack, true);
            this.client.interactionManager.dropCreativeStack(cursorStack);
            this.client.player.currentScreenHandler.setCursorStack(ItemStack.EMPTY);
        } else {
            inventory_control_tweaks$clickSlotId(inventory_control_tweaks$NO_SLOT_INDICATOR, SlotActionType.PICKUP);
        }

        if (shouldThrowMatchingStacks) {
            clickMatchingSlots(
                    this.handler.slots,
                    this.client.player,
                    inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$getDestinationInventory(),
                    cursorStack,
                    this.client.interactionManager,
                    1,
                    SlotActionType.THROW,
                    handler,
                    isCreativeInv ?
                        slot -> ((SlotAccessor)slot).inventory_control_tweaks$getIndex() :
                        slot -> slot.id);
        } else if (shouldThrowAllStacks) {
            clickAllSlots(
                    this.handler.slots,
                    this.client.player,
                    inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$getDestinationInventory(),
                    this.client.interactionManager,
                    1,
                    SlotActionType.THROW,
                    handler,
                    isCreativeInv ?
                            slot -> ((SlotAccessor)slot).inventory_control_tweaks$getIndex() :
                            slot -> slot.id
            );
        }
    }

    private void inventory_control_tweaks$onReleaseInsideBounds(@Nullable Slot hoveredSlot, ItemStack cursorStack) {
        if (
                FeatureControl.shouldDepositCursorStackOnRelease() == FeatureControl.DepositType.DISABLED ||
                !inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$hasCursorCrossedInventories()
        ) return;

        Inventory recentInventory = inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$getDestinationInventory();

        if (hoveredSlot != null) {
            if (recentInventory != hoveredSlot.inventory) return;
            if (FeatureControl.shouldDepositCursorStackOnRelease() == FeatureControl.DepositType.AT_HOVERED_SLOT) {
                final ItemStack hoveredStack = hoveredSlot.getStack();
                if (hoveredStack.isEmpty() || stacksMatch(hoveredStack, cursorStack, false))
                    inventory_control_tweaks$clickSlotId(hoveredSlot.id, SlotActionType.PICKUP);
            }
        }

        if (!cursorStack.isEmpty()) {
            // AT_EMPTY_SLOT or stack didn't fit AT_HOVERED_SLOT or hoveredSlot == null
            inventory_control_tweaks$depositRemainder(cursorStack, recentInventory);
        }
    }

    private void inventory_control_tweaks$depositRemainder(ItemStack cursorStack, Inventory recentInventory) {
        inventory_control_tweaks$insertInEmptySlot(cursorStack, recentInventory);
        if (!cursorStack.isEmpty()) {
            // cursor stack didn't fit
            final Inventory originInventory = inventory_control_tweaks$draggedAccessor.inventory_control_tweaks$getSourceInventory();
            if (recentInventory != originInventory) inventory_control_tweaks$insertInEmptySlot(cursorStack, originInventory);
        }
    }

    private void inventory_control_tweaks$insertInEmptySlot(ItemStack stack, Inventory destinationInventory) {
        final PlayerInventory playerInventory = this.client.player.getInventory();
        for (Slot slot : destinationInventory == playerInventory ? Lists.reverse(this.handler.slots) : this.handler.slots) {
            if (
                    slot.inventory == destinationInventory &&
                    (slot.inventory != playerInventory || slot.getIndex() != PlayerInventory.OFF_HAND_SLOT)
            ) {
                ItemStack slotStack = slot.getStack();
                if (slotStack.isEmpty() || (!stackIsFull(slotStack) && stacksMatch(slotStack, stack, false))) {
                    inventory_control_tweaks$clickSlotId(slot.id, SlotActionType.PICKUP);
                    if (stack.isEmpty()) break;
                }
            }
        }
    }

    private void inventory_control_tweaks$clickSlotId(int slotId, SlotActionType actionType) {
        this.client.interactionManager.clickSlot(this.handler.syncId, slotId, 0, actionType, this.client.player);
    }
}
