package net.sssubtlety.inventory_control_tweaks.mixin;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.sssubtlety.inventory_control_tweaks.FeatureControl;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.MouseDraggedMixinAccessor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.sssubtlety.inventory_control_tweaks.FeatureControl.dragAllStacksAcrossModifier;
import static net.sssubtlety.inventory_control_tweaks.Util.*;

@Mixin(HandledScreen.class)
public abstract class HandledScreenMouseDraggedMixin<T extends ScreenHandler> extends Screen implements ScreenHandlerProvider<T>, MouseDraggedMixinAccessor {
    @Shadow protected boolean cursorDragging;

    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;
    private Inventory inventory_control_tweaks$sourceInventory;
    private Inventory inventory_control_tweaks$destinationInventory;
    private boolean inventory_control_tweaks$movedStacks;
    private boolean inventory_control_tweaks$cursorCrossedInventories;

    protected HandledScreenMouseDraggedMixin(Text title) {
        super(title);
        throw new IllegalStateException("HandledScreenMouseDraggedMixin's dummy constructor called! ");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    void inventory_control_tweaks$postConstruction(CallbackInfo info) {
        this.inventory_control_tweaks$destinationInventory = null;
        this.inventory_control_tweaks$movedStacks = false;
    }

    @Inject(method = "mouseDragged", locals = LocalCapture.CAPTURE_FAILHARD, at = @At(value = "INVOKE_ASSIGN",
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;getSlotAt(DD)Lnet/minecraft/screen/slot/Slot;"))
    void inventory_control_tweaks$mouseDraggedTryMoveAll(double mouseX, double mouseY, int button, double deltaX, double deltaY, CallbackInfoReturnable<Boolean> cir, Slot hoveredSlot) {//, Slot hoveredSlot, ItemStack cursorStack) {
        if (this.client == null || this.doubleClicking || button != 0) return;

        boolean transferAll = isKeyPressed(dragAllStacksAcrossModifier, this.client);
        if (
                (transferAll ||
                FeatureControl.shouldDragMatchingStacksAcrossInventories())  &&
                (this.client.options != null) &&
                !this.client.options.getTouchscreen().get() &&
                !this.cursorDragging &&
                hoveredSlot != null
        ) {
            ItemStack cursorStack = this.handler.getCursorStack();
            if (!cursorStack.isEmpty()) {
                if (inventory_control_tweaks$destinationInventory == null) {
                    inventory_control_tweaks$sourceInventory = hoveredSlot.inventory;
                    inventory_control_tweaks$destinationInventory = inventory_control_tweaks$sourceInventory;
                } else if (
                        !(hoveredSlot.inventory instanceof CraftingInventory && ((CraftingInventoryAccessor) hoveredSlot.inventory).inventory_control_tweaks$getHandler() instanceof PlayerScreenHandler) &&
                                inventory_control_tweaks$destinationInventory != hoveredSlot.inventory) {
                    ClientPlayerEntity player = getClientPlayer();
                    if (player == null) return;

                    ClientPlayerInteractionManager interactionManager = getClientInteractionManager();
                    if (interactionManager == null) return;

                    final boolean movedStacks;
                    if (transferAll) {
                        movedStacks = clickAllSlots(
                                FeatureControl.shouldChooseBottomRowStacksFirst() ? Lists.reverse(this.handler.slots) : this.handler.slots,
                                player,
                                inventory_control_tweaks$destinationInventory,
                                interactionManager,
                                button,
                                SlotActionType.QUICK_MOVE,
                                handler,
                                slot_ -> slot_.id
                        );
                    } else {
                        movedStacks = clickMatchingSlots(
                                FeatureControl.shouldChooseBottomRowStacksFirst() ?
                                        Lists.reverse(this.handler.slots) :
                                        this.handler.slots,
                                player,
                                inventory_control_tweaks$destinationInventory,
                                cursorStack,
                                interactionManager,
                                button,
                                SlotActionType.QUICK_MOVE,
                                handler,
                                slot_ -> slot_.id
                        );
                    }
                    inventory_control_tweaks$sourceInventory = inventory_control_tweaks$destinationInventory;
                    inventory_control_tweaks$destinationInventory = hoveredSlot.inventory;
                    inventory_control_tweaks$movedStacks = movedStacks;
                    inventory_control_tweaks$cursorCrossedInventories = true;
                }
            }
        }
    }

    @Override
    public Inventory inventory_control_tweaks$getSourceInventory() {
        return inventory_control_tweaks$sourceInventory;
    }

    @Override
    public Inventory inventory_control_tweaks$getDestinationInventory() {
        return inventory_control_tweaks$destinationInventory;
    }

    @Override
    public boolean inventory_control_tweaks$hasMovedStacks() {
        return inventory_control_tweaks$movedStacks;
    }

    @Override
    public boolean inventory_control_tweaks$hasCursorCrossedInventories() {
        return inventory_control_tweaks$cursorCrossedInventories;
    }

    @Override
    public void inventory_control_tweaks$endMove() {
        inventory_control_tweaks$destinationInventory = null;
        inventory_control_tweaks$movedStacks = false;
        inventory_control_tweaks$cursorCrossedInventories = false;
    }
}
