package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.HandledScreenMouseClickedMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(CreativeInventoryScreen.class)
public class CreativeInventoryScreenMixin {
    // same mixin is in HandledScreenMouseClickedMixin because @Overrides the onMouseClick of its parent class, HandledScreen
    @Inject(method = "onMouseClick", at = @At("HEAD"), cancellable = true)
    private void inventory_control_tweaks$cancelMouseClickSlotIfReplaced(Slot slot, int slotId, int button, SlotActionType actionType, CallbackInfo ci) {
        if (((HandledScreenMouseClickedMixinAccessor)this).inventory_control_tweaks$getDidReplaceMouseClickSlot()) ci.cancel();
    }
}
