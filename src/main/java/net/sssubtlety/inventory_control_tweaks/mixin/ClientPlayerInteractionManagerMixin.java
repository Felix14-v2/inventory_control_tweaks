package net.sssubtlety.inventory_control_tweaks.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.WrapWithCondition;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.class_7204;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.sssubtlety.inventory_control_tweaks.FeatureControl;
import net.sssubtlety.inventory_control_tweaks.Util;
import org.apache.commons.lang3.mutable.MutableObject;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Environment(EnvType.CLIENT)
@Mixin(ClientPlayerInteractionManager.class)
public abstract class ClientPlayerInteractionManagerMixin {
    private boolean swappedNonArmor;

    @ModifyVariable(method = "interactItem", at = @At("STORE"))
    private MutableObject inventory_control_tweaks$initMutableObject(MutableObject mutableActionResult) {
        mutableActionResult.setValue(ActionResult.PASS);
        return mutableActionResult;
    }

    @WrapWithCondition(method = "interactItem", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;method_41931(Lnet/minecraft/client/world/ClientWorld;Lnet/minecraft/class_7204;)V"))
    private boolean inventory_control_tweaks$limitArmorSwapping(ClientPlayerInteractionManager instance, ClientWorld world, class_7204 packetProducer, PlayerEntity player, Hand hand) {
        Item item = player.getStackInHand(hand).getItem();
        return !(
                (item instanceof ArmorItem armorItem) &&
                !player.getEquippedStack(armorItem.getPreferredSlot()).isEmpty() &&
                (FeatureControl.isArmorSwapDisabled() || FeatureControl.isArmorDenied(item))
        );
    }

    @ModifyExpressionValue(
            method = "method_41929",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;use(Lnet/minecraft/world/World;Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/Hand;)Lnet/minecraft/util/TypedActionResult;")
    )
    private TypedActionResult inventory_control_tweaks$tryEquipOrSwapNonArmor(TypedActionResult original, Hand hand, PlayerEntity player, MutableObject mutableObject) {
        if (!FeatureControl.shouldEquipAndSwapNonArmorWearables()) return original;
        final ActionResult result = original.getResult();
        if (result == ActionResult.SUCCESS || result == ActionResult.CONSUME) return original;

        ItemStack usedStack = player.getStackInHand(hand);
        if (usedStack.getItem() instanceof ArmorItem) return original;

        if (Util.tryEquipItem(player, usedStack, hand == Hand.MAIN_HAND ? player.getInventory().selectedSlot : PlayerInventory.OFF_HAND_SLOT)) {
            swappedNonArmor = true;
            return new TypedActionResult(ActionResult.SUCCESS, original.getValue());
        }

        return original;
    }

    @Inject(method = "method_41931", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayNetworkHandler;sendPacket(Lnet/minecraft/network/packet/Packet;)V"))
    private void inventory_control_tweaks$cancelPacketIfSwappedNonArmor(ClientWorld world, class_7204 arg, CallbackInfo ci) {
        if (swappedNonArmor) ci.cancel();
        swappedNonArmor = false;
    }
}
