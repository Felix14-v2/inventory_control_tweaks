package net.sssubtlety.inventory_control_tweaks.mixin;

import net.sssubtlety.inventory_control_tweaks.CreativeSlotCheckable;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(targets = "net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen$CreativeSlot")
public abstract class CreativeSlotEmptyImplementer implements CreativeSlotCheckable { }
