package net.sssubtlety.inventory_control_tweaks;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.mojang.blaze3d.platform.InputUtil;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.client.option.KeyBind;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.NAMESPACE;

public class FeatureControl {
    public static final Logger LOGGER = LogManager.getLogger();

    private static final @Nullable Config CONFIG_INSTANCE;
    public static final String BR = System.lineSeparator();
    private static boolean verifySave = true;

    private static ImmutableSet<Item> armorSwapDenyList = buildItemSetFromStrings(Defaults.armor_swap_deny_list).getLeft();
    private static ImmutableSet<Item> offhandPreferredItems = buildItemSetFromStrings(Defaults.offhand_preferred_items).getLeft();

    private static ActionResult onConfigSave(ConfigHolder<Config> holder, Config config) {
        if (verifySave && invalidateConfig("Error saving config", holder, config)) {
            verifySave = false;
            holder.save();
            verifySave = true;
            return ActionResult.FAIL;
        }

        return ActionResult.CONSUME;
    }

    private static ActionResult onConfigLoad(ConfigHolder<Config> holder, Config config) {
        if (invalidateConfig("Error loading config", holder, config)) holder.save();
        return ActionResult.CONSUME;
    }

    private static boolean invalidateConfig(String errPrefix, ConfigHolder<Config> holder, Config config) {
        Pair<ImmutableSet<Item>, StringBuilder> buildResult = buildItemSetFromStrings(CONFIG_INSTANCE == null ? Defaults.armor_swap_deny_list : CONFIG_INSTANCE.armor_swap_deny_list);
        armorSwapDenyList = buildResult.getLeft();
        boolean verified = buildResult.getRight() == null;

        buildResult = buildItemSetFromStrings(CONFIG_INSTANCE == null ? Defaults.offhand_preferred_items : CONFIG_INSTANCE.offhand_preferred_items);
        offhandPreferredItems = buildResult.getLeft();
        verified &= buildResult.getRight() == null;

        if (!verified) LOGGER.error(errPrefix + ": " + "error message");
        return !verified;
    }

    private static Pair<@NotNull ImmutableSet<Item>, @Nullable StringBuilder> buildItemSetFromStrings(List<String> strings) {
        ImmutableSet.Builder<Item> itemSetBuilder = ImmutableSet.builder();
        final List<String> errIds = new LinkedList<>();
        for (String string : strings) {
            Identifier id = new Identifier(string);

            Item item = Registries.ITEM.get(id);
            if (item == Items.AIR) errIds.add(id.toString());
            else itemSetBuilder.add(item);
        }

        final int numErrs = errIds.size();
        final StringBuilder errBuilder = new StringBuilder();
        if (numErrs > 0) {
            if (numErrs == 1) errBuilder.append("No item found for id '").append(errIds.get(0)).append("'").append(BR);
            else {
                errBuilder.append("No items found for ids:").append(BR);
                for (String errId : errIds) errBuilder.append("- ").append(errId).append(BR);
            }
        }

        return new Pair<>(itemSetBuilder.build(), errBuilder.length() == 0 ? null : errBuilder);
    }

    static {
        boolean shouldLoadConfig = false;

        final Optional<ModContainer> optModContainer = FabricLoader.getInstance().getModContainer("cloth-config");
        if (optModContainer.isPresent()) {
            try {
                shouldLoadConfig = VersionPredicate.parse(">=6.1.48").test(optModContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                e.printStackTrace();
            }
        }

        if (shouldLoadConfig) {
            final ConfigHolder<Config> holder = AutoConfig.register(Config.class, GsonConfigSerializer::new);
            CONFIG_INSTANCE = holder.getConfig();
            onConfigLoad(holder, CONFIG_INSTANCE);
            holder.registerLoadListener(FeatureControl::onConfigLoad);
            holder.registerSaveListener(FeatureControl::onConfigSave);
        } else CONFIG_INSTANCE = null;
    }

    public interface Defaults {
        boolean enable_armor_swap = true;
        boolean equip_and_swap_non_armor_wearables = false;
        ImmutableList<String> armor_swap_deny_list = ImmutableList.of();
        boolean fetch_translation_updates = true;
        boolean enable_pick_fills_stack = true;
        boolean enable_pick_never_changes_slot = true;
        // drag_item_tweaks
        boolean drag_matching_stacks_across_inventories = true;
        boolean choose_bottom_row_stacks_first = true;
        DepositType deposit_cursor_stack_on_release = DepositType.AT_EMPTY_SLOT;
        boolean drag_single_stack_out_of_inventory = true;
        Keyable drag_matching_stacks_out_of_inventory = Keyable.WITH_KEY;
        boolean ignore_stack_nbt = false;
        // offhand tweaks
        boolean enable_shift_click_to_offhand_stack = true;
        boolean all_food_is_offhand_preferred = true;
        boolean exclude_food_with_negative_effects = true;
        ImmutableList<String> offhand_preferred_items = ImmutableList.of(
                Registries.ITEM.getId(Items.SHIELD).toString(),
                Registries.ITEM.getId(Items.FIREWORK_ROCKET).toString(),
                Registries.ITEM.getId(Items.TOTEM_OF_UNDYING).toString()
        );
    }

    public static final KeyBind dragMatchingOutOfInventoryModifier = KeyBindingHelper.registerKeyBinding(new KeyBind(
            "key." + NAMESPACE + ".drag_out_of_inventory_modifier",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_LEFT_CONTROL,
            "category." + NAMESPACE
    ));

    public static final KeyBind dragAllOutOfInventoryModifier = KeyBindingHelper.registerKeyBinding(new KeyBind(
            "key." + NAMESPACE + ".drag_all_out_of_inventory_modifier",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_UNKNOWN,
            "category." + NAMESPACE
    ));

    public static final KeyBind clickArmorSwapModifier = KeyBindingHelper.registerKeyBinding(new KeyBind(
            "key." + NAMESPACE + ".click_armor_swap_modifier",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_RIGHT_SHIFT,
            "category." + NAMESPACE
    ));

    public static final KeyBind dragAllStacksAcrossModifier = KeyBindingHelper.registerKeyBinding(new KeyBind(
            "key." + NAMESPACE + ".drag_all_stacks_across_modifier",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_LEFT_ALT,
            "category." + NAMESPACE
    ));

    public static boolean isArmorDenied(Item item) {
        return armorSwapDenyList.contains(item);
    }

    public static boolean isArmorSwapDisabled() {
        return !(CONFIG_INSTANCE == null ? Defaults.enable_armor_swap : CONFIG_INSTANCE.enable_armor_swap);
    }

    public static boolean shouldEquipAndSwapNonArmorWearables() {
        return (CONFIG_INSTANCE == null ? Defaults.equip_and_swap_non_armor_wearables : CONFIG_INSTANCE.equip_and_swap_non_armor_wearables);
    }

    public static boolean shouldFetchTranslationUpdates() {
        return CONFIG_INSTANCE == null ? Defaults.fetch_translation_updates : CONFIG_INSTANCE.fetch_translation_updates;
    }

    public static boolean shouldPickFillStack() {
        return CONFIG_INSTANCE == null ? Defaults.enable_pick_fills_stack : CONFIG_INSTANCE.enable_pick_fills_stack;
    }

    public static boolean shouldPickNeverChangeSlot() {
        return CONFIG_INSTANCE == null ? Defaults.enable_pick_never_changes_slot : CONFIG_INSTANCE.enable_pick_never_changes_slot;
    }

    // drag_item_tweaks
    public static boolean shouldDragMatchingStacksAcrossInventories() {
        return CONFIG_INSTANCE == null ? Defaults.drag_matching_stacks_across_inventories : CONFIG_INSTANCE.drag_matching_stacks_across_inventories;
    }

    public static boolean shouldChooseBottomRowStacksFirst() {
        return CONFIG_INSTANCE == null ? Defaults.choose_bottom_row_stacks_first : CONFIG_INSTANCE.choose_bottom_row_stacks_first;
    }

    public static DepositType shouldDepositCursorStackOnRelease() {
        return CONFIG_INSTANCE == null ? Defaults.deposit_cursor_stack_on_release : CONFIG_INSTANCE.deposit_cursor_stack_on_release;
    }

    public static boolean shouldDragSingleStackOutOfInventory() {
        return CONFIG_INSTANCE == null ? Defaults.drag_single_stack_out_of_inventory : CONFIG_INSTANCE.drag_single_stack_out_of_inventory;
    }

    public static Keyable shouldDragMatchingStacksOutOfInventory() {
        return CONFIG_INSTANCE == null ? Defaults.drag_matching_stacks_out_of_inventory : CONFIG_INSTANCE.drag_matching_stacks_out_of_inventory;
    }

    public static boolean shouldIgnoreStackNbt() {
        return CONFIG_INSTANCE == null ? Defaults.ignore_stack_nbt : CONFIG_INSTANCE.ignore_stack_nbt;
    }

    // offhand tweaks
    public static boolean shouldShiftClickToOffhandStack() {
        return CONFIG_INSTANCE == null ? Defaults.enable_shift_click_to_offhand_stack : CONFIG_INSTANCE.enable_shift_click_to_offhand_stack;
    }

    public static boolean isAllFoodOffhandPreferred() {
        return CONFIG_INSTANCE == null ? Defaults.all_food_is_offhand_preferred : CONFIG_INSTANCE.all_food_is_offhand_preferred;
    }

    public static boolean shouldExcludeFoodWithNegativeEffects() {
        return CONFIG_INSTANCE == null ? Defaults.exclude_food_with_negative_effects : CONFIG_INSTANCE.exclude_food_with_negative_effects;
    }

    public static boolean isOffhandPreferred(Item item) {
        return offhandPreferredItems.contains(item);
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static void init() { }

    public enum DepositType {
        DISABLED,
        AT_HOVERED_SLOT,
        AT_EMPTY_SLOT
    }

    @SuppressWarnings("unused")
    public enum Keyable {
        DISABLED,
        NO_KEY,
        WITH_KEY
    }

    private FeatureControl() { }
}
