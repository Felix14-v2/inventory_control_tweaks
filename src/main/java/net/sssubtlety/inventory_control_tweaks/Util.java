package net.sssubtlety.inventory_control_tweaks;

import com.mojang.blaze3d.platform.InputUtil;
import com.mojang.datafixers.util.Pair;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.option.KeyBind;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.sssubtlety.inventory_control_tweaks.mixin.LivingEntityAccessor;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper.getBoundKeyOf;

public final class Util {

    public static ClientPlayerEntity getClientPlayer() {
        return MinecraftClient.getInstance().player;
    }

    public static ClientPlayerInteractionManager getClientInteractionManager() {
        return MinecraftClient.getInstance().interactionManager;
    }

    public static int getFirstHotbarInd() {
        return PlayerInventory.MAIN_SIZE;
    }

    public static boolean stackIsFull(ItemStack stack) {
        return stack.getCount() == stack.getMaxCount();
    }

    public static boolean stacksMatch(ItemStack stackA, ItemStack stackB, boolean ignoreNbt) {
        return ItemStack.itemsMatch(stackA, stackB) &&
                (ignoreNbt || ItemStack.canCombine(stackA, stackB));
    }

    public static int getClickedSlotId(int slotInd) {
        return slotInd < PlayerInventory.getHotbarSize() ? slotInd + PlayerInventory.MAIN_SIZE : slotInd;
    }

    public static int getClickedStackInd(int slotInd) {
        // index: 9-35, 0-8
        // clickedStackInd: 9-35, 36-44
        final int firstHotbarInd = getFirstHotbarInd();
        return slotInd >= firstHotbarInd ? slotInd - firstHotbarInd : slotInd;
    }

    public static boolean isValidFood(Item item) {
        boolean isValid = item.isFood();
        if(isValid && FeatureControl.shouldExcludeFoodWithNegativeEffects() && item != Items.SUSPICIOUS_STEW) {
            // item.isFood() does item.getFoodComponent() != null, so below is safe access
            List<Pair<StatusEffectInstance, Float>> statusEffects = item.getFoodComponent().getStatusEffects();
            if (statusEffects != null) {
                for (Pair<net.minecraft.entity.effect.StatusEffectInstance, Float> pair : statusEffects) {
                    if (!pair.getFirst().getEffectType().isBeneficial()) {
                        isValid = false;
                        break;
                    }
                }
            }
        }
        return isValid;
    }

    public static void combineStacks(ClientPlayerEntity player, ClientPlayerInteractionManager interactionManager, int fromSlotId, int toIndex) {
        // should only be called of stacks are already known to match
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, toIndex, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
    }

    // returns true iff slots change as a result of clicking
    public static boolean clickMatchingSlots(List<Slot> slots, ClientPlayerEntity player, Inventory inventory, ItemStack referenceStack, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Function<Slot, Integer> indexer) {
        final Predicate<ItemStack> stackPredicate = stack -> stacksMatch(referenceStack, stack, FeatureControl.shouldIgnoreStackNbt()) && isStackNotWorn(player, stack);
        return clickEachSlot(slots, player, inventory, interactionManager, button, action, handler, indexer, stackPredicate);
    }

    // returns true iff slots change as a result of clicking
    public static boolean clickAllSlots(List<Slot> slots, ClientPlayerEntity player, Inventory inventory, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Function<Slot, Integer> indexer) {
        final Predicate<ItemStack> itemStackPredicate = stack -> isStackNotWorn(player, stack);
        return clickEachSlot(slots, player, inventory, interactionManager, button, action, handler, indexer, itemStackPredicate);
    }

    // returns true iff slots change as a result of clicking
    private static boolean clickEachSlot(List<Slot> slots, ClientPlayerEntity player, Inventory inventory, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Function<Slot, Integer> indexer, Predicate<ItemStack> stackPredicate) {
        boolean slotsChanged = false;
        for (Slot slot : slots) {
            slotsChanged |= trySlotClick(player, inventory, interactionManager, button, action, handler, slot, stackPredicate, indexer);
        }
        return slotsChanged;
    }

    // returns true iff slot changes as a result of clicking
    public static boolean trySlotClick(ClientPlayerEntity player, Inventory inventory, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Slot slot, Predicate<ItemStack> stackPredicate, Function<Slot, Integer> indexer) {
        boolean slotChanged = false;
        if (
                slot != null &&
                slot.canTakeItems(player) &&
                slot.hasStack() &&
                slot.inventory == inventory
        ) {
            ItemStack curStack = slot.getStack();
            if (stackPredicate.test(curStack) && ScreenHandler.canInsertItemIntoSlot(slot, curStack, true)) {
                ItemStack oldStack = curStack.copy();
                interactionManager.clickSlot(handler.syncId, indexer.apply(slot), button, action, player);
                slotChanged = !oldStack.equals(slot.getStack());
            }
        }
        return slotChanged;
    }

    public static boolean isStackNotWorn(ClientPlayerEntity player, ItemStack stack) {
        for (ItemStack offhandStack : player.getInventory().offHand) if (stack == offhandStack) return false;
        for (ItemStack armorStack : player.getInventory().armor) if (stack == armorStack) return false;
        return true;
    }

    public static boolean tryEquipItem(PlayerEntity player, ItemStack usedStack, int sourceSlotId) {
        if (FeatureControl.isArmorDenied(usedStack.getItem())) return false;

        EquipmentSlot equipmentSlot = MobEntity.getPreferredEquipmentSlot(usedStack);
        if (equipmentSlot.getType() != EquipmentSlot.Type.ARMOR) return false;

        if (FeatureControl.isArmorSwapDisabled() && !player.getEquippedStack(equipmentSlot).isEmpty()) return false;

        ClientPlayerInteractionManager interactionManager = MinecraftClient.getInstance().interactionManager;
        if (interactionManager == null) return false;

        int equipmentSlotId = equipmentSlot.getEntitySlotId();

        interactionManager.clickSlot(player.playerScreenHandler.syncId, 8 - equipmentSlotId, sourceSlotId,
                SlotActionType.SWAP, player);
        ((LivingEntityAccessor) player).inventory_control_tweaks$callProcessEquippedStack(usedStack);
        return true;
    }

    public static boolean isKeyPressed(KeyBind key, MinecraftClient client) {
        return InputUtil.isKeyPressed(client.getWindow().getHandle(), getBoundKeyOf(key).getKeyCode());
    }

    @SuppressWarnings("ConstantConditions")
    public static int getRgb(Text text) {
        final TextColor color = text.getStyle().getColor();
        if (color == null) return Formatting.WHITE.getColorValue();
        else return color.getRgb();
    }

    public static Text replace(Text text, String regex, String replacement) {
        String string = text.getString();
        string = string.replaceAll(regex, replacement);
        return Text.literal(string).setStyle(text.getStyle());
    }

    record TranslatableString(String key) {
        public String get() {
            return I18n.translate(key);
        }
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        final Optional<ModContainer> optModContainer = FabricLoader.getInstance().getModContainer(id);
        if (optModContainer.isPresent()){
            try {
                return VersionPredicate.parse(versionPredicate).test(optModContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private Util() { }
}
